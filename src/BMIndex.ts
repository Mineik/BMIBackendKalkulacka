export function getBMI(weight: number, height: number){
	const BMI = calculateBMI(weight, height)
	const rating = getRating(BMI)
	const textRatin = getTextRating(rating)

	return {
		step: rating,
		text: textRatin,
		value: BMI,
	}
}


function getRating(BMI: number){
	if(BMI > 40){
		return 0
	}else if(BMI >= 35 && BMI < 40){
		return 1
	}else if(BMI >= 30 && BMI < 35){
		return 2
	}else if(BMI >= 25 && BMI < 30){
		return 3
	}else if(BMI >= 18.5 && BMI < 25){
		return 4
	}else if(BMI >= 16.5 && BMI < 18.5){
		return 5
	}else{return 6}
}

function calculateBMI(weight: number, height: number){
	return weight/Math.pow(height/100,2)
}

function getTextRating(rating: number){
	switch(rating) {
		case 0:
			return "Obezita třetího stupně"
		case 1:
			return "Obezita druhého stupně"
		case 2:
			return "Obezita prvního stupně"
		case 3:
			return "Nadváha"
		case 4:
			return "Zdravá váha"
		case 5:
			return "Podváha"
		case 6:
			return "Těžká podvýživa"
		default:
			return "Chyba"
	}
}