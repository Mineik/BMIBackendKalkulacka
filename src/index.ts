import {ApolloServer, gql} from "apollo-server"
import {getBMI} from "./BMIndex"

const schema = gql`
	
	type BMI{
		step: Int
		value: Float!
		text: String
	}
	
	type Query{
		BMI(weight: Int!, height: Int!): BMI
	}
`

const resolver = {
	Query: {
		BMI(parent: any, args: any, context: any, info: any){
			console.log(args)
			return getBMI(args.weight, args.height)
		}
	}
}

const server = new ApolloServer({typeDefs:schema, resolvers:resolver})

server.listen({port:3000}).then(e=>console.log("Listening at: "+e.port))